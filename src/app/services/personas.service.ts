import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Persona } from '../classes/persona';
import { environment } from '../../environments/environment';
import { ResDel } from '../interfaces/res-del';
@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  persona: Persona;
  url: string = environment.url;

  constructor(private http: HttpClient) { }

  getPersonas() {
    const url = `${this.url}/personas`;
    return this.http.get<Persona[]>(url);
  }

  savePersona(persona: Persona) {
    const url = `${this.url}/personas`;

    const promise = new Promise((resolve, reject) => {
      this.http.post(url, persona)
        .subscribe(res => {
          resolve(res);
        }, error => {
          reject(error);
        });
    });
    return promise;
  }

  updatePersona(persona: Persona) {
    const url = `${this.url}/personas/${persona.id}`;

    const promise = new Promise((resolve, reject) => {
      this.http.put(url, persona)
        .subscribe(res => {
          resolve(res);
        }, error => {
          reject(error);
        });
    });
    return promise;
  }

  getPersona(id: any) {
    const url = `${this.url}/personas/${id}`;
    return this.http.get<Persona>(url);
  }

  deletePersona(id: any) {
    const url = `${this.url}/personas/${id}`;
    return this.http.delete<ResDel>(url);
  }
}
