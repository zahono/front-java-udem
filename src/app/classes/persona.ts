export class Persona {
  id: number;
  nombres: string;
  apellidos: string;
  documento: number;
  telefono?: number;

  constructor(id: number, nombres: string, apellidos: string, documento: number, telefono?: number) {
    this.id = id;
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.documento = documento;
    this.telefono = telefono;
  }
}
