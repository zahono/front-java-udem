import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faList, faUserPlus, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  title = 'Prueba Java Frontend';
  faList = faList;
  faUserPlus = faUserPlus;
  faSignOutAlt = faSignOutAlt;

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  List() {
    this.route.navigateByUrl('/personas');
  }

  New() {
    this.route.navigateByUrl('/add');
  }

}
