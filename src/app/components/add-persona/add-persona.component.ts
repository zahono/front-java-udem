import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PersonasService } from './../../services/personas.service';
import { Title, Meta, MetaDefinition } from '@angular/platform-browser';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { faSave } from '@fortawesome/free-regular-svg-icons';
import { faEraser } from '@fortawesome/free-solid-svg-icons';
import { Persona } from '../../classes/persona';

@Component({
  selector: 'app-add-persona',
  templateUrl: './add-persona.component.html',
  styles: [
  ]
})
export class AddPersonaComponent implements OnInit {

  personaForm: FormGroup;
  sended = false;
  faSave = faSave;
  faEraser = faEraser;

  constructor(
    private formBuilder: FormBuilder,
    private personasService: PersonasService,
    private title: Title,
    private meta: Meta,
    private loc: Location
  ) {
    this.title.setTitle('Agregar');

    const metaTag: MetaDefinition = {
      name: 'description',
      content: `Se encuentra en la página para realizar la agregación de personas`
    };

    this.meta.updateTag(metaTag);

    this.personaForm = this.formBuilder.group({
      id: [null],
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      documento: ['', Validators.required],
      telefono: [''],
    });
  }

  ngOnInit(): void { }

  clean() {
    this.sended = false;
    this.personaForm.reset();
  }

  send() {
    this.sended = true;
    if (this.personaForm.valid) {
      console.log(this.personaForm.value);
      this.personasService.savePersona(this.personaForm.value).then(
        (res: Persona) => {
          if (res) {
            Swal.fire({
              title: 'Solicitud exitosa',
              text: `La persona ${res?.nombres} ${res?.apellidos} ha sido creada con éxito.`,
              icon: 'success',
              confirmButtonText: 'Ok',
              confirmButtonColor: '#a5dc86'
            }).then(resp => {
              if (resp.value) {
                this.clean();
              } else if (resp.dismiss) {
                this.clean();
              }
              this.loc.back();
            });
          } else {
            Swal.fire({
              title: 'Error',
              text: `No se pudo registrar a la persona correctamente.`,
              icon: 'error',
              confirmButtonText: 'Ok',
              confirmButtonColor: '#f27474'
            });
          }
        },
        error => {
          Swal.fire({
            title: 'Error',
            text: `${error.message}`,
            icon: 'error',
            confirmButtonText: 'Ok',
            confirmButtonColor: '#f27474'
          });
        }
      );
    }
  }

}
