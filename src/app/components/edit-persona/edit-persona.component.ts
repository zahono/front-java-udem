import { Component, OnInit } from '@angular/core';
import { Title, Meta, MetaDefinition } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { PersonasService } from '../../services/personas.service';
import Swal from 'sweetalert2';
import { faSave } from '@fortawesome/free-regular-svg-icons';
import { faEraser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-edit-persona',
  templateUrl: './edit-persona.component.html',
  styles: [
  ]
})
export class EditPersonaComponent implements OnInit {
  id: number;
  editForm: FormGroup;
  sended = false;
  faSave = faSave;
  faEraser = faEraser;

  constructor(
    private route: ActivatedRoute,
    private personasService: PersonasService,
    private formBuilder: FormBuilder,
    private title: Title,
    private meta: Meta,
    private loc: Location
  ) {
    this.id = this.route.snapshot.params.id;

    this.title.setTitle('Editar');

    const metaTag: MetaDefinition = {
      name: 'description',
      content: `Se encuentra en la página para realizar la edición de personas`
    };

    this.meta.updateTag(metaTag);
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      id: [null],
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      documento: ['', Validators.required],
      telefono: [''],
    });

    this.personasService.getPersona(this.id).subscribe(person => {
      this.editForm.patchValue(person);
    });
  }

  clean() {
    this.sended = false;
    this.editForm.reset();
  }

  send() {
    this.sended = true;
    if (this.editForm.valid) {
      console.log(this.editForm.value);
      this.personasService.updatePersona(this.editForm.value).then(
        res => {
          if (res) {
            Swal.fire({
              title: 'Solicitud exitosa',
              text: `La persona ${res['nombres']} ${res['apellidos']} ha sido editada con éxito.`,
              icon: 'success',
              confirmButtonText: 'Ok',
              confirmButtonColor: '#a5dc86'
            }).then(resp => {
              if (resp.value) {
                this.clean();
              } else if (resp.dismiss) {
                this.clean();
              }
              this.loc.back();
            });
          } else {
            Swal.fire({
              title: 'Error',
              text: `No se pudo editar a la persona correctamente.`,
              icon: 'error',
              confirmButtonText: 'Ok',
              confirmButtonColor: '#f27474'
            });
          }
        },
        error => {
          Swal.fire({
            title: 'Error',
            text: `${error['message']}`,
            icon: 'error',
            confirmButtonText: 'Ok',
            confirmButtonColor: '#f27474'
          });
        }
      );
    }
  }
}
