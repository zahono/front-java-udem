import { Component, OnInit } from '@angular/core';
import { PersonasService } from './../../services/personas.service';
import { Persona } from '../../classes/persona';
import { Router } from '@angular/router';
import { Meta, Title, MetaDefinition } from '@angular/platform-browser';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styles: [
  ]
})
export class PersonasComponent implements OnInit {

  personas: Persona[] = [];
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;

  constructor(private personasService: PersonasService, private router: Router, private meta: Meta, private title: Title, private loc: Location) {
    this.title.setTitle('Personas');

    const metaTag: MetaDefinition = {
      name: 'description',
      content: `Se encuentra en la vista de las personas registradas`
    };

    this.meta.updateTag(metaTag);
  }

  ngOnInit() {
    this.personasService.getPersonas()
      .subscribe((personas: Persona[]) => {
        this.personas = personas;
      });
  }

  edit(id: number) {
    this.router.navigateByUrl(`/edit/${id}`);
  }

  delete(id: number) {
    this.personasService.deletePersona(id)
      .subscribe(response => {
        if (response['status'] === 'success') {
          Swal.fire({
            title: response['status'],
            text: response['message'],
            icon: 'success',
            confirmButtonText: 'Ok',
            confirmButtonColor: '#a5dc86'
          });
          this.personas = [];
          this.ngOnInit();
        } else {
          Swal.fire({
            title: response['status'],
            text: response['message'],
            icon: 'error',
            confirmButtonText: 'Ok',
            confirmButtonColor: '#f27474'
          });
        }
      })
  }

}
