import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { HeaderComponent } from './header/header.component';
import { PersonasComponent } from './personas/personas.component';
import { AddPersonaComponent } from './add-persona/add-persona.component';
import { EditPersonaComponent } from './edit-persona/edit-persona.component';
import { NotFoundComponent } from './not-found/not-found.component';



@NgModule({
  declarations: [HeaderComponent, PersonasComponent, AddPersonaComponent, EditPersonaComponent, NotFoundComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  exports: [HeaderComponent, PersonasComponent, AddPersonaComponent, EditPersonaComponent, NotFoundComponent],
})
export class ComponentsModule { }
