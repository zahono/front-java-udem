export const environment = {
  production: true,
  url: 'http://localhost:8080/back_java_udem_shn/api/v1',
  emailPattern: /^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/
};
